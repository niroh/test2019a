// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBKiPbDiCyvEchEHMXhNCFWnaFTNfWN0-8",
    authDomain: "test2019a-c2807.firebaseapp.com",
    databaseURL: "https://test2019a-c2807.firebaseio.com",
    projectId: "test2019a-c2807",
    storageBucket: "test2019a-c2807.appspot.com",
    messagingSenderId: "1098394638527",
    appId: "1:1098394638527:web:8ac4621bf5e00e8ed28e0a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
