import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(public db:AngularFirestore) { }

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'A Song Of Ice And Fire', author:'George R R Martin'}]

  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges({idField:'id'});
  }  

  addBook(title:string, author:string){
    const book = {title:title, author:author}
    this.db.collection('books').add(book);
  }

  updateBook(id:string, title:string, author:string){
    this.db.doc(`books/${id}`).update({
      title:title,
      author:author
    })
  }

  deleteBook(id:string){
    this.db.doc(`books/${id}`).delete();
  }

  


}
