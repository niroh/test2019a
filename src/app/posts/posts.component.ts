import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Posts } from '../interfaces/posts';
import { Comments } from '../interfaces/comments';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(public postsservice:PostsService) { }

  posts$: Posts[];
  comments$: Comments[];

  ngOnInit() {
    this.postsservice.getPosts().subscribe(
      data => {
        this.posts$ = data;
       // console.log(data)
      }
    );
    this.postsservice.getComments().subscribe(
      data1 => {
        this.comments$ = data1;
       // console.log(data1)
      }
    );
   }

}
