import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Posts } from './interfaces/posts';
import { Comments } from './interfaces/comments';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(public http:HttpClient) { }

  public URLposts = "https://jsonplaceholder.typicode.com/posts/"
  public URLcomments = "http://jsonplaceholder.typicode.com/comments/"

  getPosts(){
    return this.http.get<Posts[]>(this.URLposts)
  }

  getComments(){
    return this.http.get<Comments[]>(this.URLcomments)
  }
}
