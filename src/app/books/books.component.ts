import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(public booksservice:BooksService) { }

  books$:Observable<any>;

  deleteBook(id:string){
    this.booksservice.deleteBook(id);
  }

  ngOnInit() {
    this.books$ = this.booksservice.getBooks();
  }

}
