import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  
  constructor(private booksservice:BooksService, 
              private router:Router,
              private route:ActivatedRoute) { }

  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Book"


  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
      console.log(this.id);
      if(this.id){
        this.isEdit = true;
        this.buttonText = "Update Book"
        
  
      }
  
  }


  onSubmit(){
    this.booksservice.addBook(this.title, this.author)
    this.router.navigate(['/books']);
  }

}