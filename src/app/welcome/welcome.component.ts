import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(    public authservice:AuthService,
                  public router:Router,
                  public activatedroute:ActivatedRoute) { }
  
   user: Observable<User | null>
   
   users$: User[];

  ngOnInit() {
  }

}
