import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  constructor(public classifyservice:ClassifyService,
              public imageservice:ImageService) { }

  category:string = "Loading...";
  categoryImage:string;


  ngOnInit() {
    this.classifyservice.classify().subscribe(
      res => {
        this.category = this.classifyservice.categories[res];
        this.categoryImage = this.imageservice.images[res];
      }
    )
  }


}
